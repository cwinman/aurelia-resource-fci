import {Resource} from './resource'

let resources = {}

class ResourceData {

    endpoint = ''
    hateoasAssociations = {}

    constructor(target) {
        this.target = target
    }

    addEndpoint(endpoint) {
        this.endpoint = endpoint
    }

    addHateoasAssociation(propertyName, resourceName) {
        this.hateoasAssociations[propertyName] = resourceName
    }

}

export function getResource(name) {
    return resources[name]
}

export function getResourceForKey(key) {
    let captializedKey = key.charAt(0).toUpperCase() + key.slice(1)
    let uppercasedKey = key.toUpperCase()
    let singularKey = key.slice(0, -1)

    if (resources[captializedKey]) return Resource.for(captializedKey)
    if (resources[uppercasedKey]) return Resource.for(uppercasedKey)
    if (resources[singularKey]) return Resource.for(singularKey)

    return
}

export function registerResource(name, target) {
    if (!(name in resources)) resources[name] = new ResourceData(target)

    if (!('populate' in resources[name].target)) resources[name].target = target

    return resources[name]
}
