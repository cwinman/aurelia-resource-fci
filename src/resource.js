import isPlural from 'is-plural'
import {apiConfig} from './config'
import {debug} from './debug'
import {getResource, registerResource, getResourceForKey} from './resource-registry'
import {Response, ResponseCollection} from './response'
import {mergeEmbeddedObjects, getHateoasLink} from './utils/hateoas'

let cachedResources = {}

// @resource decorator
export function resource(endpoint) {
    return function(target) {
        let resource = registerResource(target.name, new target())
        resource.addEndpoint(endpoint)
    }
}

// @association decorator
export function association(resourceName='') {
    return function(target, propertyName, descriptor) {
        descriptor.configurable = true

        let resource = registerResource(target.constructor.name, target)
        resource.addHateoasAssociation(propertyName, resourceName)
    }
}

// Resource class, used through @inject(Resource.for('User'))
export class Resource {

    requestTimers = {}

    constructor(name, target, endpoint) {
        this.name = name
        this.target = target
        this.endpoint = endpoint
    }

    get api() {
        // TODO: api shouldn't be hardcoded here
        return apiConfig.api.endpoints['api']
    }

    find(params={}) {
        let parsedEndpoint = this.endpoint.replace(/\//g, '')

        debug(`is ${parsedEndpoint} plural? `, isPlural(parsedEndpoint))

        if (!isPlural(parsedEndpoint)) return this.findOne()

        debug('find all ', this.endpoint)

        return this.fetch(this.endpoint, null, params, true)
    }

    findOne(params={}) {
        debug('find one ', this.endpoint)

        return this.fetch(this.endpoint, null, params)
    }

    get(id) {
        if (!id) throw new ReferenceError('Must pass ID to get method')

        return this.fetch(this.endpoint, id)
    }

    save(resource) {
        return this.api.update(this.endpoint, null, this._filterResource(resource))
    }

    create(link, resource, links) {
        if (!links || !links[link]) throw new ReferenceError('Failed while attempting POST. ${link} was not found in HATEOAS links for resource.')

        return this.api.create(links[link].href, this._filterResource(resource))
    }

    delete(links) {
        if (!links || !links['self']) throw new ReferenceError('Failed while attempting DELETE. Resource HATEOAS links does not include self link.')

        return this.api.request('DELETE', links['self'].href)
    }

    fetch(resource, id, params={}, isCollection=false) {
        let response = isCollection ? new ResponseCollection() : this._createResponseFromData()

        response.promise = new Promise((resolve, reject) => {
            if (id) resource = `${resource}/${id}`

            var urlParams = Object.keys(params).map(key => `${key}=${encodeURIComponent(params[key])}`).join('&')

            if (urlParams !== '') resource = `${resource}?${urlParams}`

            console.debug('fetch ', resource)

            this.api.find(resource).then(result => {
                response = this.constructResponseFromData(result, response, isCollection)

                resolve(response)
            }, err => reject(err))
        })

        this._addGettersToResponseForPromiseMethods(response)

        return response
    }

    constructResponseFromData(data, response=null, isCollection=false) {
        let mergedData = mergeEmbeddedObjects(data, isCollection)

        if (!response) {
            response = isCollection ? new ResponseCollection() : this._createResponseFromData()

            // this is kind of weird, but right now the resource is expected to have a promise,
            // but we already have data, so create the promise and immediately resolve
            response.promise = new Promise((resolve, reject) => resolve(response))
        }

        this._populateResponseWithResult(response, mergedData, isCollection)
        this._addResources(mergedData)

        return response
    }

    _addResources(result) {
        let resource = getResource(this.name)

        for (var key in resource.hateoasAssociations) {
            let associatedResource = getResource(resource.hateoasAssociations[key])
            let hateoasLink = getHateoasLink(result, key)

            if (!associatedResource) throw new ReferenceError(`${resource.hateoasAssociations[key]} resource doesn't exist`)

            this[key] = new Resource(resource.hateoasAssociations[key], associatedResource.target, hateoasLink)
        }
    }

    _createResponseFromData(data) {
        let resource = getResource(this.name)

        let response = resource ? new resource.target.constructor() : new Response()

        if (data) response.populate(data)

        response.resource = this

        let handler = {
            get (target, key) {
                let associatedResourceKey = resource.hateoasAssociations[key]

                if (!target[key] && associatedResourceKey) {
                    response.resource._triggerHateoasLinkOnResponse(target, key, associatedResourceKey, response)
                }

                return target[key]
            }
        }

        response = this._removeNonExistentHateoasProperties(response, resource.hateoasAssociations)

        return new Proxy(response, handler)
    }

    _removeNonExistentHateoasProperties(response, hateoasAssociations) {
        for (var key in hateoasAssociations) {
            if (!response._links || !response._links[key]) {
                delete response[key]
            }
        }

        return response
    }

    _triggerHateoasLinkOnResponse(target, key, associatedResourceKey, response) {
        let hateoasLink = getHateoasLink(response, key)

        // wait for the link to exist before continuing
        if (!hateoasLink) {
            if (associatedResourceKey in response.resource.requestTimers) {
                clearTimeout(response.resource.requestTimers[associatedResourceKey])
            }

            response.resource.requestTimers[associatedResourceKey] = setTimeout(() => {
                response.resource._triggerHateoasLinkOnResponse(target, key, associatedResourceKey, response)
            }, 50)

            return
        }

        let associatedResource = getResource(associatedResourceKey)

        if (!associatedResource) throw new ReferenceError(`${associatedResource} resource doesn't exist`)

        target[key] = new Resource(associatedResourceKey, associatedResource.target, hateoasLink).find()
    }

    _populateResponseWithResult(response, result, isCollection) {
        if (isCollection) {
            if (!result.length) return

            Array.prototype.push.apply(response, result.map(object => this._createResponseFromData(object)))
        } else {
            response.populate(result)
        }
    }

    _filterResource(resource) {
        return this._filterObject(resource, key => key.substr(0, 1) !== '_' && !['resource', 'promise'].includes(key))
    }

    _filterArray(arr, predicate) {
        return arr.map(obj => this._filterObject(obj, predicate))
    }

    _filterObject(obj, predicate) {
        if (!obj || typeof obj !== 'object') return obj

        if (Array.isArray(obj)) return this._filterArray(obj, predicate)

        return Object.keys(obj)
            .filter(key => predicate(key, obj[key]))
            .reduce((res, key) => (res[key] = this._filterObject(obj[key], predicate), res), {})
    }

    _addGettersToResponseForPromiseMethods(response) {
        Object.defineProperty(response, 'isResolved', { get: function () { return this.promise.isResolved() } })
        Object.defineProperty(response, 'isRejected', { get: function () { return this.promise.isRejected() } })
    }

    static for(resourceName) {
        if (!(resourceName in cachedResources)) {
            let resource = getResource(resourceName)

            if (!resource) throw new ReferenceError(`${resourceName} resource doesn't exist`)

            cachedResources[resourceName] = new Resource(resourceName, resource.target, resource.endpoint)
        }

        return cachedResources[resourceName]
    }

}
