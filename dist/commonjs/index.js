'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.configure = configure;

var _config = require('./config');

Object.keys(_config).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function get() {
            return _config[key];
        }
    });
});

var _resource = require('./resource');

Object.keys(_resource).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function get() {
            return _resource[key];
        }
    });
});

var _response = require('./response');

Object.keys(_response).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function get() {
            return _response[key];
        }
    });
});
function configure(aurelia, configCallback) {
    if (configCallback === undefined || typeof configCallback !== 'function') return;

    configCallback(aurelia.container.get(_config.Config));
}