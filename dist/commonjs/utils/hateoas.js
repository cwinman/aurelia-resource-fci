'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.mergeEmbeddedObjects = mergeEmbeddedObjects;
exports.getEmbeddedObjects = getEmbeddedObjects;
exports.hasEmbeddedObjects = hasEmbeddedObjects;
exports.getHateoasLink = getHateoasLink;

var _resourceRegistry = require('../resource-registry');

var _debug = require('../debug');

function mergeEmbeddedObjects(result) {
    var isCollection = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    if (!result || (typeof result === 'undefined' ? 'undefined' : _typeof(result)) !== 'object') return result;

    (0, _debug.debug)('result passed to mergeEmbeddedObjects: ', result);
    (0, _debug.debug)('is collection? ', isCollection);

    var embeddedObjects = getEmbeddedObjects(result);
    var embeddedObjectsKeys = Object.keys(embeddedObjects);

    (0, _debug.debug)('embedded object keys ', embeddedObjectsKeys);

    if (embeddedObjectsKeys.length > 0 && isCollection) {
        result = embeddedObjects[embeddedObjectsKeys[0]];
    } else {
        for (var key in embeddedObjects) {
            var resource = (0, _resourceRegistry.getResourceForKey)(key);

            if (resource) {
                embeddedObjects[key] = resource.constructResponseFromData(embeddedObjects[key]);
            }

            result[key] = embeddedObjects[key];
        }
    }

    (0, _debug.debug)('result merged with embedded objects if there are any: ', result);

    return result;
}

function getEmbeddedObjects(result) {
    if (!hasEmbeddedObjects(result) || _typeof(result._embedded) !== 'object') return {};

    return result._embedded;
}

function hasEmbeddedObjects(result) {
    if (!result || (typeof result === 'undefined' ? 'undefined' : _typeof(result)) !== 'object') return false;

    return '_embedded' in result;
}

function getHateoasLink(result, key) {
    if (!('_links' in result)) return;

    var hyphenatedKey = camelCaseToHyphenated(key);
    var link = result._links[key] || result._links[hyphenatedKey];

    if (!link) return;

    return link.href;
}

function isHateoasProperty(key) {
    return key === '_embedded' || key === '_links';
}

function camelCaseToHyphenated(key) {
    return key.replace(/([a-z][A-Z])/g, function (g) {
        return g[0] + '-' + g[1].toLowerCase();
    });
}