'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.debug = debug;

var _config = require('./config');

var PREPEND = 'DEBUG [aurelia-resource] ';

function debug(message) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

    if (!_config.apiConfig.debug) return;

    if (data) {
        console.debug(PREPEND + ' ' + message, data);
    } else {
        console.debug(PREPEND + ' ' + message);
    }
}