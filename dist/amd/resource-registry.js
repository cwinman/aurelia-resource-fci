define(['exports', './resource'], function (exports, _resource) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.getResource = getResource;
    exports.getResourceForKey = getResourceForKey;
    exports.registerResource = registerResource;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var resources = {};

    var ResourceData = function () {
        function ResourceData(target) {
            _classCallCheck(this, ResourceData);

            this.endpoint = '';
            this.hateoasAssociations = {};

            this.target = target;
        }

        ResourceData.prototype.addEndpoint = function addEndpoint(endpoint) {
            this.endpoint = endpoint;
        };

        ResourceData.prototype.addHateoasAssociation = function addHateoasAssociation(propertyName, resourceName) {
            this.hateoasAssociations[propertyName] = resourceName;
        };

        return ResourceData;
    }();

    function getResource(name) {
        return resources[name];
    }

    function getResourceForKey(key) {
        var captializedKey = key.charAt(0).toUpperCase() + key.slice(1);
        var uppercasedKey = key.toUpperCase();
        var singularKey = key.slice(0, -1);

        if (resources[captializedKey]) return _resource.Resource.for(captializedKey);
        if (resources[uppercasedKey]) return _resource.Resource.for(uppercasedKey);
        if (resources[singularKey]) return _resource.Resource.for(singularKey);

        return;
    }

    function registerResource(name, target) {
        if (!(name in resources)) resources[name] = new ResourceData(target);

        if (!('populate' in resources[name].target)) resources[name].target = target;

        return resources[name];
    }
});