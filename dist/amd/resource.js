define(['exports', 'is-plural', './config', './debug', './resource-registry', './response', './utils/hateoas'], function (exports, _isPlural, _config, _debug, _resourceRegistry, _response, _hateoas) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.Resource = undefined;
    exports.resource = resource;
    exports.association = association;

    var _isPlural2 = _interopRequireDefault(_isPlural);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
        return typeof obj;
    } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    var cachedResources = {};

    function resource(endpoint) {
        return function (target) {
            var resource = (0, _resourceRegistry.registerResource)(target.name, new target());
            resource.addEndpoint(endpoint);
        };
    }

    function association() {
        var resourceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        return function (target, propertyName, descriptor) {
            descriptor.configurable = true;

            var resource = (0, _resourceRegistry.registerResource)(target.constructor.name, target);
            resource.addHateoasAssociation(propertyName, resourceName);
        };
    }

    var Resource = exports.Resource = function () {
        function Resource(name, target, endpoint) {
            _classCallCheck(this, Resource);

            this.requestTimers = {};

            this.name = name;
            this.target = target;
            this.endpoint = endpoint;
        }

        Resource.prototype.find = function find() {
            var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            var parsedEndpoint = this.endpoint.replace(/\//g, '');

            (0, _debug.debug)('is ' + parsedEndpoint + ' plural? ', (0, _isPlural2.default)(parsedEndpoint));

            if (!(0, _isPlural2.default)(parsedEndpoint)) return this.findOne();

            (0, _debug.debug)('find all ', this.endpoint);

            return this.fetch(this.endpoint, null, params, true);
        };

        Resource.prototype.findOne = function findOne() {
            var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            (0, _debug.debug)('find one ', this.endpoint);

            return this.fetch(this.endpoint, null, params);
        };

        Resource.prototype.get = function get(id) {
            if (!id) throw new ReferenceError('Must pass ID to get method');

            return this.fetch(this.endpoint, id);
        };

        Resource.prototype.save = function save(resource) {
            return this.api.update(this.endpoint, null, this._filterResource(resource));
        };

        Resource.prototype.create = function create(link, resource, links) {
            if (!links || !links[link]) throw new ReferenceError('Failed while attempting POST. ${link} was not found in HATEOAS links for resource.');

            return this.api.create(links[link].href, this._filterResource(resource));
        };

        Resource.prototype.delete = function _delete(links) {
            if (!links || !links['self']) throw new ReferenceError('Failed while attempting DELETE. Resource HATEOAS links does not include self link.');

            return this.api.request('DELETE', links['self'].href);
        };

        Resource.prototype.fetch = function fetch(resource, id) {
            var _this = this;

            var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
            var isCollection = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

            var response = isCollection ? new _response.ResponseCollection() : this._createResponseFromData();

            response.promise = new Promise(function (resolve, reject) {
                if (id) resource = resource + '/' + id;

                var urlParams = Object.keys(params).map(function (key) {
                    return key + '=' + encodeURIComponent(params[key]);
                }).join('&');

                if (urlParams !== '') resource = resource + '?' + urlParams;

                console.debug('fetch ', resource);

                _this.api.find(resource).then(function (result) {
                    response = _this.constructResponseFromData(result, response, isCollection);

                    resolve(response);
                }, function (err) {
                    return reject(err);
                });
            });

            this._addGettersToResponseForPromiseMethods(response);

            return response;
        };

        Resource.prototype.constructResponseFromData = function constructResponseFromData(data) {
            var response = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var isCollection = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

            var mergedData = (0, _hateoas.mergeEmbeddedObjects)(data, isCollection);

            if (!response) {
                response = isCollection ? new _response.ResponseCollection() : this._createResponseFromData();

                response.promise = new Promise(function (resolve, reject) {
                    return resolve(response);
                });
            }

            this._populateResponseWithResult(response, mergedData, isCollection);
            this._addResources(mergedData);

            return response;
        };

        Resource.prototype._addResources = function _addResources(result) {
            var resource = (0, _resourceRegistry.getResource)(this.name);

            for (var key in resource.hateoasAssociations) {
                var associatedResource = (0, _resourceRegistry.getResource)(resource.hateoasAssociations[key]);
                var hateoasLink = (0, _hateoas.getHateoasLink)(result, key);

                if (!associatedResource) throw new ReferenceError(resource.hateoasAssociations[key] + ' resource doesn\'t exist');

                this[key] = new Resource(resource.hateoasAssociations[key], associatedResource.target, hateoasLink);
            }
        };

        Resource.prototype._createResponseFromData = function _createResponseFromData(data) {
            var resource = (0, _resourceRegistry.getResource)(this.name);

            var response = resource ? new resource.target.constructor() : new _response.Response();

            if (data) response.populate(data);

            response.resource = this;

            var handler = {
                get: function get(target, key) {
                    var associatedResourceKey = resource.hateoasAssociations[key];

                    if (!target[key] && associatedResourceKey) {
                        response.resource._triggerHateoasLinkOnResponse(target, key, associatedResourceKey, response);
                    }

                    return target[key];
                }
            };

            response = this._removeNonExistentHateoasProperties(response, resource.hateoasAssociations);

            return new Proxy(response, handler);
        };

        Resource.prototype._removeNonExistentHateoasProperties = function _removeNonExistentHateoasProperties(response, hateoasAssociations) {
            for (var key in hateoasAssociations) {
                if (!response._links || !response._links[key]) {
                    delete response[key];
                }
            }

            return response;
        };

        Resource.prototype._triggerHateoasLinkOnResponse = function _triggerHateoasLinkOnResponse(target, key, associatedResourceKey, response) {
            var hateoasLink = (0, _hateoas.getHateoasLink)(response, key);

            if (!hateoasLink) {
                if (associatedResourceKey in response.resource.requestTimers) {
                    clearTimeout(response.resource.requestTimers[associatedResourceKey]);
                }

                response.resource.requestTimers[associatedResourceKey] = setTimeout(function () {
                    response.resource._triggerHateoasLinkOnResponse(target, key, associatedResourceKey, response);
                }, 50);

                return;
            }

            var associatedResource = (0, _resourceRegistry.getResource)(associatedResourceKey);

            if (!associatedResource) throw new ReferenceError(associatedResource + ' resource doesn\'t exist');

            target[key] = new Resource(associatedResourceKey, associatedResource.target, hateoasLink).find();
        };

        Resource.prototype._populateResponseWithResult = function _populateResponseWithResult(response, result, isCollection) {
            var _this2 = this;

            if (isCollection) {
                if (!result.length) return;

                Array.prototype.push.apply(response, result.map(function (object) {
                    return _this2._createResponseFromData(object);
                }));
            } else {
                response.populate(result);
            }
        };

        Resource.prototype._filterResource = function _filterResource(resource) {
            return this._filterObject(resource, function (key) {
                return key.substr(0, 1) !== '_' && !['resource', 'promise'].includes(key);
            });
        };

        Resource.prototype._filterArray = function _filterArray(arr, predicate) {
            var _this3 = this;

            return arr.map(function (obj) {
                return _this3._filterObject(obj, predicate);
            });
        };

        Resource.prototype._filterObject = function _filterObject(obj, predicate) {
            var _this4 = this;

            if (!obj || (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object') return obj;

            if (Array.isArray(obj)) return this._filterArray(obj, predicate);

            return Object.keys(obj).filter(function (key) {
                return predicate(key, obj[key]);
            }).reduce(function (res, key) {
                return res[key] = _this4._filterObject(obj[key], predicate), res;
            }, {});
        };

        Resource.prototype._addGettersToResponseForPromiseMethods = function _addGettersToResponseForPromiseMethods(response) {
            Object.defineProperty(response, 'isResolved', { get: function get() {
                    return this.promise.isResolved();
                } });
            Object.defineProperty(response, 'isRejected', { get: function get() {
                    return this.promise.isRejected();
                } });
        };

        Resource.for = function _for(resourceName) {
            if (!(resourceName in cachedResources)) {
                var _resource = (0, _resourceRegistry.getResource)(resourceName);

                if (!_resource) throw new ReferenceError(resourceName + ' resource doesn\'t exist');

                cachedResources[resourceName] = new Resource(resourceName, _resource.target, _resource.endpoint);
            }

            return cachedResources[resourceName];
        };

        _createClass(Resource, [{
            key: 'api',
            get: function get() {
                return _config.apiConfig.api.endpoints['api'];
            }
        }]);

        return Resource;
    }();
});