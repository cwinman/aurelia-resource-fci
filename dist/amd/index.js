define(['exports', './config', './resource', './response'], function (exports, _config, _resource, _response) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.configure = configure;
    Object.keys(_config).forEach(function (key) {
        if (key === "default" || key === "__esModule") return;
        Object.defineProperty(exports, key, {
            enumerable: true,
            get: function () {
                return _config[key];
            }
        });
    });
    Object.keys(_resource).forEach(function (key) {
        if (key === "default" || key === "__esModule") return;
        Object.defineProperty(exports, key, {
            enumerable: true,
            get: function () {
                return _resource[key];
            }
        });
    });
    Object.keys(_response).forEach(function (key) {
        if (key === "default" || key === "__esModule") return;
        Object.defineProperty(exports, key, {
            enumerable: true,
            get: function () {
                return _response[key];
            }
        });
    });
    function configure(aurelia, configCallback) {
        if (configCallback === undefined || typeof configCallback !== 'function') return;

        configCallback(aurelia.container.get(_config.Config));
    }
});