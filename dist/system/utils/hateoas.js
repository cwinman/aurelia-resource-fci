'use strict';

System.register(['../resource-registry', '../debug'], function (_export, _context) {
    "use strict";

    var getResourceForKey, debug, _typeof;

    function mergeEmbeddedObjects(result) {
        var isCollection = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

        if (!result || (typeof result === 'undefined' ? 'undefined' : _typeof(result)) !== 'object') return result;

        debug('result passed to mergeEmbeddedObjects: ', result);
        debug('is collection? ', isCollection);

        var embeddedObjects = getEmbeddedObjects(result);
        var embeddedObjectsKeys = Object.keys(embeddedObjects);

        debug('embedded object keys ', embeddedObjectsKeys);

        if (embeddedObjectsKeys.length > 0 && isCollection) {
            result = embeddedObjects[embeddedObjectsKeys[0]];
        } else {
            for (var key in embeddedObjects) {
                var resource = getResourceForKey(key);

                if (resource) {
                    embeddedObjects[key] = resource.constructResponseFromData(embeddedObjects[key]);
                }

                result[key] = embeddedObjects[key];
            }
        }

        debug('result merged with embedded objects if there are any: ', result);

        return result;
    }

    _export('mergeEmbeddedObjects', mergeEmbeddedObjects);

    function getEmbeddedObjects(result) {
        if (!hasEmbeddedObjects(result) || _typeof(result._embedded) !== 'object') return {};

        return result._embedded;
    }

    _export('getEmbeddedObjects', getEmbeddedObjects);

    function hasEmbeddedObjects(result) {
        if (!result || (typeof result === 'undefined' ? 'undefined' : _typeof(result)) !== 'object') return false;

        return '_embedded' in result;
    }

    _export('hasEmbeddedObjects', hasEmbeddedObjects);

    function getHateoasLink(result, key) {
        if (!('_links' in result)) return;

        var hyphenatedKey = camelCaseToHyphenated(key);
        var link = result._links[key] || result._links[hyphenatedKey];

        if (!link) return;

        return link.href;
    }

    _export('getHateoasLink', getHateoasLink);

    function isHateoasProperty(key) {
        return key === '_embedded' || key === '_links';
    }

    function camelCaseToHyphenated(key) {
        return key.replace(/([a-z][A-Z])/g, function (g) {
            return g[0] + '-' + g[1].toLowerCase();
        });
    }
    return {
        setters: [function (_resourceRegistry) {
            getResourceForKey = _resourceRegistry.getResourceForKey;
        }, function (_debug) {
            debug = _debug.debug;
        }],
        execute: function () {
            _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
                return typeof obj;
            } : function (obj) {
                return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
            };
        }
    };
});