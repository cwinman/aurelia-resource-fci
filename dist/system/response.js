'use strict';

System.register([], function (_export, _context) {
    "use strict";

    var _createClass, ResponseCollection, Response;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    return {
        setters: [],
        execute: function () {
            _createClass = function () {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || false;
                        descriptor.configurable = true;
                        if ("value" in descriptor) descriptor.writable = true;
                        Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }

                return function (Constructor, protoProps, staticProps) {
                    if (protoProps) defineProperties(Constructor.prototype, protoProps);
                    if (staticProps) defineProperties(Constructor, staticProps);
                    return Constructor;
                };
            }();

            _export('ResponseCollection', ResponseCollection = function (_Array) {
                _inherits(ResponseCollection, _Array);

                function ResponseCollection() {
                    _classCallCheck(this, ResponseCollection);

                    return _possibleConstructorReturn(this, _Array.call(this));
                }

                return ResponseCollection;
            }(Array));

            _export('ResponseCollection', ResponseCollection);

            _export('Response', Response = function () {
                function Response() {
                    _classCallCheck(this, Response);
                }

                Response.prototype.populate = function populate(data) {
                    Object.assign(this, data);

                    this.addEmbeddedPropertyAccessors();
                };

                Response.prototype.addEmbeddedPropertyAccessors = function addEmbeddedPropertyAccessors() {
                    if (!this.hasEmbeddedProperties) return;

                    for (var key in this._embedded) {
                        this[key] = this._embedded[key];
                    }
                };

                Response.prototype.save = function save() {
                    return this.resource.save(this);
                };

                Response.prototype.create = function create(link, resource) {
                    return this.resource.create(link, resource, this._links || {});
                };

                Response.prototype.delete = function _delete() {
                    return this.resource.delete(this._links || {});
                };

                _createClass(Response, [{
                    key: 'hasEmbeddedProperties',
                    get: function get() {
                        return '_embedded' in this;
                    }
                }]);

                return Response;
            }());

            _export('Response', Response);
        }
    };
});