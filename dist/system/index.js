'use strict';

System.register(['./config', './resource', './response'], function (_export, _context) {
    "use strict";

    var Config;
    function configure(aurelia, configCallback) {
        if (configCallback === undefined || typeof configCallback !== 'function') return;

        configCallback(aurelia.container.get(Config));
    }

    _export('configure', configure);

    return {
        setters: [function (_config) {
            Config = _config.Config;
            var _exportObj = {};

            for (var _key in _config) {
                if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _config[_key];
            }

            _export(_exportObj);
        }, function (_resource) {
            var _exportObj2 = {};

            for (var _key2 in _resource) {
                if (_key2 !== "default" && _key2 !== "__esModule") _exportObj2[_key2] = _resource[_key2];
            }

            _export(_exportObj2);
        }, function (_response) {
            var _exportObj3 = {};

            for (var _key3 in _response) {
                if (_key3 !== "default" && _key3 !== "__esModule") _exportObj3[_key3] = _response[_key3];
            }

            _export(_exportObj3);
        }],
        execute: function () {}
    };
});