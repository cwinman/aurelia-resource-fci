'use strict';

System.register(['./config'], function (_export, _context) {
    "use strict";

    var apiConfig, PREPEND;
    function debug(message) {
        var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        if (!apiConfig.debug) return;

        if (data) {
            console.debug(PREPEND + ' ' + message, data);
        } else {
            console.debug(PREPEND + ' ' + message);
        }
    }

    _export('debug', debug);

    return {
        setters: [function (_config) {
            apiConfig = _config.apiConfig;
        }],
        execute: function () {
            PREPEND = 'DEBUG [aurelia-resource] ';
        }
    };
});