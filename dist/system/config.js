'use strict';

System.register([], function (_export, _context) {
    "use strict";

    var _createClass, apiConfig, Config;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    return {
        setters: [],
        execute: function () {
            _createClass = function () {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || false;
                        descriptor.configurable = true;
                        if ("value" in descriptor) descriptor.writable = true;
                        Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }

                return function (Constructor, protoProps, staticProps) {
                    if (protoProps) defineProperties(Constructor.prototype, protoProps);
                    if (staticProps) defineProperties(Constructor, staticProps);
                    return Constructor;
                };
            }();

            _export('apiConfig', apiConfig = {
                api: null,
                apiName: 'api',
                apiUrl: '/api/',
                authAndRedirectUrl: '/api/auth-and-redirect/',
                debug: false
            });

            _export('apiConfig', apiConfig);

            _export('Config', Config = function () {
                function Config() {
                    _classCallCheck(this, Config);
                }

                Config.prototype.configure = function configure(api, incomingConfig, ErrorService) {
                    _export('apiConfig', apiConfig = Object.assign(apiConfig, incomingConfig));
                    apiConfig.api = api;

                    this.addEndpoints();

                    this.errorService = ErrorService;
                };

                Config.prototype.addEndpoints = function addEndpoints() {
                    var _this = this;

                    apiConfig.api.registerEndpoint(apiConfig.apiName, function (config) {
                        config.withBaseUrl(apiConfig.apiUrl).withDefaults({
                            credentials: 'same-origin',
                            headers: {
                                'Accept': 'application/json'
                            }
                        }).withInterceptor(_this.interceptors);
                    });
                };

                Config.prototype.requestInterceptor = function requestInterceptor(request) {
                    return request;
                };

                Config.prototype.responseInterceptor = function responseInterceptor(response) {
                    this.errorService.prototype.consumeError(response);
                    return response;
                };

                _createClass(Config, [{
                    key: 'loginRedirect',
                    get: function get() {
                        return apiConfig.authAndRedirectUrl + '?url=' + window.location.href.replace(window.location.hash, '');
                    }
                }, {
                    key: 'interceptors',
                    get: function get() {
                        var _this2 = this;

                        return {
                            request: function request(_request) {
                                return _this2.requestInterceptor(_request);
                            },
                            response: function response(_response) {
                                return _this2.responseInterceptor(_response);
                            }
                        };
                    }
                }]);

                return Config;
            }());

            _export('Config', Config);
        }
    };
});