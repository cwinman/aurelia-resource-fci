'use strict';

System.register(['./resource'], function (_export, _context) {
    "use strict";

    var Resource, resources, ResourceData;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    function getResource(name) {
        return resources[name];
    }

    _export('getResource', getResource);

    function getResourceForKey(key) {
        var captializedKey = key.charAt(0).toUpperCase() + key.slice(1);
        var uppercasedKey = key.toUpperCase();
        var singularKey = key.slice(0, -1);

        if (resources[captializedKey]) return Resource.for(captializedKey);
        if (resources[uppercasedKey]) return Resource.for(uppercasedKey);
        if (resources[singularKey]) return Resource.for(singularKey);

        return;
    }

    _export('getResourceForKey', getResourceForKey);

    function registerResource(name, target) {
        if (!(name in resources)) resources[name] = new ResourceData(target);

        if (!('populate' in resources[name].target)) resources[name].target = target;

        return resources[name];
    }

    _export('registerResource', registerResource);

    return {
        setters: [function (_resource) {
            Resource = _resource.Resource;
        }],
        execute: function () {
            resources = {};

            ResourceData = function () {
                function ResourceData(target) {
                    _classCallCheck(this, ResourceData);

                    this.endpoint = '';
                    this.hateoasAssociations = {};

                    this.target = target;
                }

                ResourceData.prototype.addEndpoint = function addEndpoint(endpoint) {
                    this.endpoint = endpoint;
                };

                ResourceData.prototype.addHateoasAssociation = function addHateoasAssociation(propertyName, resourceName) {
                    this.hateoasAssociations[propertyName] = resourceName;
                };

                return ResourceData;
            }();
        }
    };
});