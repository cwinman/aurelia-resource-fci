import { apiConfig } from './config';

const PREPEND = 'DEBUG [aurelia-resource] ';

export function debug(message, data = null) {
    if (!apiConfig.debug) return;

    if (data) {
        console.debug(`${PREPEND} ${message}`, data);
    } else {
        console.debug(`${PREPEND} ${message}`);
    }
}