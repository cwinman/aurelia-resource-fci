import { getResourceForKey } from '../resource-registry';
import { debug } from '../debug';

export function mergeEmbeddedObjects(result, isCollection = false) {
    if (!result || typeof result !== 'object') return result;

    debug('result passed to mergeEmbeddedObjects: ', result);
    debug('is collection? ', isCollection);

    let embeddedObjects = getEmbeddedObjects(result);
    let embeddedObjectsKeys = Object.keys(embeddedObjects);

    debug('embedded object keys ', embeddedObjectsKeys);

    if (embeddedObjectsKeys.length > 0 && isCollection) {
        result = embeddedObjects[embeddedObjectsKeys[0]];
    } else {
        for (let key in embeddedObjects) {
            let resource = getResourceForKey(key);

            if (resource) {
                embeddedObjects[key] = resource.constructResponseFromData(embeddedObjects[key]);
            }

            result[key] = embeddedObjects[key];
        }
    }

    debug('result merged with embedded objects if there are any: ', result);

    return result;
}

export function getEmbeddedObjects(result) {
    if (!hasEmbeddedObjects(result) || typeof result._embedded !== 'object') return {};

    return result._embedded;
}

export function hasEmbeddedObjects(result) {
    if (!result || typeof result !== 'object') return false;

    return '_embedded' in result;
}

export function getHateoasLink(result, key) {
    if (!('_links' in result)) return;

    let hyphenatedKey = camelCaseToHyphenated(key);
    let link = result._links[key] || result._links[hyphenatedKey];

    if (!link) return;

    return link.href;
}

function isHateoasProperty(key) {
    return key === '_embedded' || key === '_links';
}

function camelCaseToHyphenated(key) {
    return key.replace(/([a-z][A-Z])/g, g => `${g[0]}-${g[1].toLowerCase()}`);
}