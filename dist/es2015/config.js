export let apiConfig = {
    api: null,
    apiName: 'api',
    apiUrl: '/api/',
    authAndRedirectUrl: '/api/auth-and-redirect/',
    debug: false
};

export let Config = class Config {

    get loginRedirect() {
        return `${apiConfig.authAndRedirectUrl}?url=${window.location.href.replace(window.location.hash, '')}`;
    }

    get interceptors() {
        return {
            request: request => this.requestInterceptor(request),
            response: response => this.responseInterceptor(response)
        };
    }

    configure(api, incomingConfig, ErrorService) {
        apiConfig = Object.assign(apiConfig, incomingConfig);
        apiConfig.api = api;

        this.addEndpoints();

        this.errorService = ErrorService;
    }

    addEndpoints() {
        apiConfig.api.registerEndpoint(apiConfig.apiName, config => {
            config.withBaseUrl(apiConfig.apiUrl).withDefaults({
                credentials: 'same-origin',
                headers: {
                    'Accept': 'application/json'
                }
            }).withInterceptor(this.interceptors);
        });
    }

    requestInterceptor(request) {
        return request;
    }

    responseInterceptor(response) {
        this.errorService.prototype.consumeError(response);
        return response;
    }
};