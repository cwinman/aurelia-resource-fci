export let ResponseCollection = class ResponseCollection extends Array {

    constructor() {
        super();
    }

};

export let Response = class Response {

    populate(data) {
        Object.assign(this, data);

        this.addEmbeddedPropertyAccessors();
    }

    get hasEmbeddedProperties() {
        return '_embedded' in this;
    }

    addEmbeddedPropertyAccessors() {
        if (!this.hasEmbeddedProperties) return;

        for (var key in this._embedded) {
            this[key] = this._embedded[key];
        }
    }

    save() {
        return this.resource.save(this);
    }

    create(link, resource) {
        return this.resource.create(link, resource, this._links || {});
    }

    delete() {
        return this.resource.delete(this._links || {});
    }

};